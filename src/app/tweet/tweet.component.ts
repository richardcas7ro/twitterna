import { Component, OnInit } from '@angular/core';
import { Tweet } from '../models/tweet.model';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.css']
})
export class TweetComponent implements OnInit {

  listaTweets = [];


  minewTweet: Tweet;

  constructor() {
    this.init();
  }


  addtweeter() {
    this.listaTweets.push(this.minewTweet);
    this.init();
  }
  
  init(){
    this.minewTweet = new Tweet();
  }

  ngOnInit(): void {
  }

}
